import React from "react";
import { Provider } from "react-redux";
import { Router, Route, Switch } from "react-router-dom";
import history from "../src/common/history";
import store from "./store";
import { Home, Navbar } from "../src/Components";

function App() {
  return (
    <Provider store={store}>
      <Router history={history}>
        <Navbar />
        <Switch>
          <Route exact path="/" component={Home} />
          {/* <Route
            path="/"
            name="Home"
            render={props => <DefaultLayout {...props} />}
          /> */}
        </Switch>
      </Router>
    </Provider>
  );
}

export default App;
