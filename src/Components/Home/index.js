import React from "react";
import { person } from "../../assets/images";
import {
  Title,
  Wrapper,
  Element,
  Image,
  Container,
  Col,
  Content,
  Span,
  SubTitle,
  SubContent,
  Button,
  Anchor,
  Row,
  AboutCard
} from "./wraperStyle";

function Home() {
  return (
    <>
      <Wrapper>
        <Element>
          <Image src={person} alt="person" />
        </Element>
        <Container>
          <Col>
            <Content>
              <Span>Hello I Am</Span>
              <Title>Aman Thakur</Title>
              <SubTitle>An Professional UI/UX Designer</SubTitle>
              <SubContent>
                Lorem Ipsum is simply dummy text of the printing and typesetting
                industry. Lorem Ipsum has been the industry's standard dummy
                text ever since the 1500s, when an unknown printer took a galley
                of type and scrambled it to make a type specimen book. It has
                survived not only five centuries, but also the leap into
                electronic typesetting, remaining essentially unchanged. It was
                popularised in the 1960s with the release of Letraset sheets
                containing Lorem Ipsum passages, and more recently with desktop
                publishing software like Aldus PageMaker including versions of
                Lorem Ipsum.
              </SubContent>
              <Button>
                <Anchor href="#">Know About Me </Anchor>
              </Button>
            </Content>
          </Col>
        </Container>
      </Wrapper>
      <Wrapper>
        <Container right="auto" left="auto">
          <Row>
            {/* <Col>
              <div>
                <AboutCard>
                  <Image src={person} alt="person" />
                </AboutCard>
              </div>
            </Col> */}
            <Col>
              <Content>
                <Span>Hello I Am</Span>
                <Title>Aman Thakur</Title>
                <SubTitle>An Professional UI/UX Designer</SubTitle>
                <SubContent>
                  Lorem Ipsum is simply dummy text of the printing and
                  typesetting industry. Lorem Ipsum has been the industry's
                  standard dummy text ever since the 1500s, when an unknown
                  printer took a galley of type and scrambled it to make a type
                  specimen book. It has survived not only five centuries, but
                  also the leap into electronic typesetting, remaining
                  essentially unchanged. It was popularised in the 1960s with
                  the release of Letraset sheets containing Lorem Ipsum
                  passages, and more recently with desktop publishing software
                  like Aldus PageMaker including versions of Lorem Ipsum.
                </SubContent>
                <Button>
                  <Anchor href="#">Know About Me </Anchor>
                </Button>
              </Content>
            </Col>
          </Row>
        </Container>
      </Wrapper>
    </>
  );
}

export default Home;
