import styled from "styled-components";

export const Title = styled.h2`
  font-size: 70px;
  font-weight: 700;
  margin-bottom: 15px;
  color: white;
  text-transform: capitalize;
  font-family: "Roboto Slab", serif;
  position: relative;
  padding-left: 80px;
  ::before {
    position: absolute;
    content: "";
    top: 50%;
    left: 0;
    width: 60px;
    height: 5px;
    background: #fff;
  }
`;

export const SubTitle = styled.h3`
  margin-bottom: 30px;
  font-size: 30px;
  font-family: "Poppins", sans-serif;
  font-weight: 500;
  color: #fff;
  text-shadow: 1px 1px 1px black;
`;

export const SubContent = styled.p`
  color: #dfdfdf;
  font-size: 18px;
  text-transform: unset;
  font-family: "Roboto Slab", sans-serif;
  font-weight: 400;
  width: 87%;
  position: relative;
`;

export const Button = styled.div`
  display: flex;
  margin-top: 50px;
`;

export const Anchor = styled.a`
  box-shadow: -3px -3px 8px rgba(34, 34, 53, 0.95),
    3px 3px 8px rgba(12, 11, 14, 0.82);
  background-color: transparent;
  font-family: "Roboto Slab", sans-serif;
  text-decoration: none;
  color: #fff;
  font-weight: 600;
  text-transform: capitalize;
  /* box-shadow: -3px -3px 8px rgba(255, 255, 255, 0.95),
    3px 3px 8px rgba(0, 0, 0, 0.1); */
  border-color: white;
  display: inline-block;
  font-size: 18px;
  border-radius: 25px;
  padding: 15px 30px;
  overflow: hidden;
  z-index: 999999;
`;

export const Wrapper = styled.section`
  position: relative;
  /* overflow: hidden; */
  background: #18182f;
  display: block;
  z-index: 2;
`;

export const Element = styled.div`
  position: absolute;
  right: 70px;
  top: 79px;
  z-index: 3;
  background: #18182f;
`;

export const Image = styled.img`
  max-width: 100%;
  height: auto;
  vertical-align: middle;
  border-style: none;
  width: 100%;
  @media only screen and (max-width: 1199px) {
    display: none;
  }
`;
export const Container = styled.div`
  width: 100%;
  height: 100vh;
  padding-right: 15px;
  padding-left: 15px;
  margin-right: auto;
  margin-left: auto;
  display: flex;
  -ms-flex-wrap: wrap;
  flex-wrap: wrap;
  margin-right: ${props => (props.right ? props.right : "-15px")};
  margin-left: ${props => (props.left ? props.left : "-15px")};
  /* align-items: center !important; */
`;

export const Col = styled.div`
  flex: 0 0 66.666667%;
  max-width: 66.666667%;
`;

export const Content = styled.div`
  position: relative;
  z-index: 9;
  margin-left: 6rem;
  margin-top: 3rem;
`;

export const Span = styled.span`
  color: #6449e7;
  font-family: "Roboto Slab", sans-serif;
  font-size: 20px;
  font-weight: 500;
  margin-bottom: 10px;
  position: relative;
  border-radius: 1rem;
`;

export const Row = styled.div`
  display: flex;
  -ms-flex-wrap: wrap;
  flex-wrap: wrap;
  margin-right: -15px;
  margin-left: -15px;
  justify-content: center !important;
  margin-bottom: -30px;
  align-items: center !important;
`;

export const AboutCard = styled.div`
  box-shadow: -5px -5px 10px rgba(255, 255, 255, 0.05),
    5px 5px 10px rgba(0, 0, 0, 0.3);
  background-color: #fff;
  position: relative;
  border-radius: 10px;
  transition: all 0.7s;
  position: relative;
  padding-right: 15px;
  padding-left: 15px;
`;
