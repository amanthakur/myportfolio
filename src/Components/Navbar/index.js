import React from "react";
import {
  Header,
  LogoContainer,
  Logo,
  Image,
  Nav,
  ListWraper,
  List,
  AnchorTag
} from "./navbarWraper";
import { logo, search } from "../../assets/images";

function Navbar() {
  return (
    <Header>
      <LogoContainer>
        <Image src={logo} alt="logo" />
        <Logo>man</Logo>
      </LogoContainer>
      <Nav>
        <ListWraper>
          <List>
            <AnchorTag href="">Home</AnchorTag>
          </List>
          <List>
            <AnchorTag href="">About</AnchorTag>
          </List>
          <List>
            <AnchorTag href="">Services</AnchorTag>
          </List>
          <List>
            <AnchorTag href="">Portfolio</AnchorTag>
          </List>
          <List>
            <AnchorTag href="">Blog</AnchorTag>
          </List>
          <List>
            <AnchorTag href="">Contact</AnchorTag>
          </List>
          <List>
            <Image src={search} alt="logo" height={`1.3rem`} />
          </List>
        </ListWraper>
      </Nav>
    </Header>
  );
}

export default Navbar;
