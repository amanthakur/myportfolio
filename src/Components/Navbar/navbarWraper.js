import styled from "styled-components";

export const Header = styled.header`
  display: flex;
  /* width: 90%; */
  margin: auto;
  position: relative;
  align-items: center;
  background: #19182f;
  padding-left: 2em;
  padding-right: 2em;
  z-index: 99;
  ::before {
    box-shadow: inset -3px -3px 8px rgba(34, 34, 53, 0.95),
      inset 3px 3px 8px rgba(12, 11, 14, 0.82);
    background-color: #18182f;
    position: absolute;
    content: "";
    bottom: 0;
    left: 0;
    width: 100%;
    height: 5px;
  }
  ::after {
    box-shadow: inset -3px -3px 8px rgba(34, 34, 53, 0.95),
      inset 3px 3px 8px rgba(12, 11, 14, 0.82);
    background-color: #18182f;
  }
`;

export const LogoContainer = styled.div`
  flex: 1;
  display: flex;
  padding-left: 4rem;
`;

export const Image = styled.img`
  height: ${props => (props.height ? props.height : "2rem")};
  margin: 5px;
  color: white;
`;

export const Logo = styled.h4`
  font-weight: bolder;
  font-family: "Roboto Slab", serif;
  margin-right: 5px;
  margin-top: 7px;
  margin-bottom: 5px;
  font-size: 24px;
  color: #fff;
  &:hover {
    color: #6348e7;
  }
`;

export const Nav = styled.nav`
  flex: 2;
`;

export const ListWraper = styled.ul`
  display: flex;
  justify-content: space-evenly;
`;

export const List = styled.li`
  display: flex;
  position: relative;
  padding: 12px 30px 12px 30px;
  box-shadow: -3px -3px 8px rgba(34, 34, 53, 0.95),
    3px 3px 8px rgba(12, 11, 14, 0.82);
  color: white;
  border-radius: 10px;
  &:hover {
    display: flex;
    position: relative;
    padding: 12px 30px 12px 30px;
    color: white;
    border-radius: 3px;
    border-bottom: 3px solid;
    border-bottom-color: #6348e7;
    transition: width 0.3s;

    /* border-radius: 3px; */
    /* display: block; */
  }
`;

export const AnchorTag = styled.a`
  text-decoration: none;
  font-family: "Roboto Slab", serif;
  font-size: 20px;
  font-weight: bold;
  color: white;
`;
