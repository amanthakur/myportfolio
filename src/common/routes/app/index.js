import React from "react";

const Second = React.lazy(() => import("../../../Components"));

const routes = [
  {
    path: `${process.env.PUBLIC_URL}/home`,
    exact: true,
    name: "Home",
    component: Second
  }
];

export default routes;
